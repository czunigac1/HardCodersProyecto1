//Autor jOSE sOLANO


//Cargar Dropdows
loadDropdowns();

//Vartiables universales
document.querySelector("#createEvent").addEventListener('click', saveEvent);
var  eventToCreate  =  document.querySelector("#eventName");
var  dateToBegintEvet  =  document.querySelector("#dateBegin");
var  dateToEndEvent  =  document.querySelector("#dateEnd");
var  priceInsert  =  document.querySelector("#priceIns");
var  placeOfEvet  =  document.querySelector("#place");
var  pirceTicket  =  document.querySelector("#priceEnter");
var  evetType  =  document.querySelector("#evetType");
var capacity = document.querySelector("#available");

//Agregar Ewventos
eventToCreate.addEventListener('change', changeBoder);
dateToBegintEvet.addEventListener('change', changeBoder);
dateToEndEvent.addEventListener('change', changeBoder);
priceInsert.addEventListener('change',changeBoder );
placeOfEvet.addEventListener('change', validatePlace);
pirceTicket.addEventListener('change', changeBoder);
evetType.addEventListener('change', changeBoder);
capacity.addEventListener('change', valdiateCapacity);
capacity.disabled = true;




var errors = false;
var idEvent = 0;

/*Funcionalidad*/
function loadDropdowns(){
	loadEventType();
	loadPlace();
}

function loadEventType(){
  var eventListType = getEventTypeList();
  var select = document.querySelector('#evetType');

  var optionType = document.createElement('option');
  optionType.value = '';
  optionType.text = 'Seleccione un tipo de evento';
  select.appendChild(optionType);

  for(var i = 0; i < eventListType.length; i++){
    var opcion = document.createElement('option');
    opcion.value = eventListType[i];
    opcion.text = eventListType[i];
    select.appendChild(opcion);
  }
}

function loadPlace () {
  var placeListType = getPlaceList();
  var select = document.querySelector('#place');

  var optionType = document.createElement('option');
  optionType.value = '';
  optionType.text = 'Seleccione un lugar';
  select.appendChild(optionType);

  for(var i = 0; i < placeListType.length; i++){
    var opcion = document.createElement('option');
    opcion.value = placeListType[i][0];
    opcion.text = placeListType[i][1];
    select.appendChild(opcion);
  }
}

function saveEvent () {
	var valueEvet = eventToCreate.value;
	var valueDateToBegintEvet = dateToBegintEvet.value;
	var valueDatoEndEvent = dateToEndEvent.value;
	var valuePriceInset = priceInsert.value;
	var valuePlaceOfEvet = placeOfEvet.value;
	var valuePirceTicket = pirceTicket.value;
	var valueEvetType = evetType.value;
	var valueCapacity = capacity.value;

	var continueSaving = validateNull();
	var validateNumbersForm = validateInputNumbers(priceInsert,pirceTicket,capacity);
	if(continueSaving || errors || validateNumbersForm){
		printMessageError();
		return;
	}
	var events = [idEvent, valueEvet,valueDateToBegintEvet,valueDatoEndEvent,valuePriceInset, valuePlaceOfEvet, valueCapacity, valuePirceTicket, valueEvetType, 0, true];

	createNewEvent(events);

	Redirect();
}


function validateNull(){
	var errosFind =false;
	var inputValues = [eventToCreate,
					  dateToBegintEvet,
					  dateToEndEvent,
					  priceInsert,
					  placeOfEvet,
					  pirceTicket,
					 evetType,
					 capacity];

	for (var i = 0; i < inputValues.length; i++) {
		if(!validateText(inputValues[i].value)){
			showError(inputValues[i]);
			errosFind =true;
		}
	};
	return errosFind;
}

function showError(inputError){
    inputError.classList.add('errorInput');
}

function changeBoder(){
	this.classList.remove('errorInput');
}

function validatePlace(){
	this.classList.remove('errorInput');
	capacity.disabled = false;
}

function valdiateCapacity(){
	var capasityRequested = Number(this.value);
	var place =document.querySelector("#place").value;
	var result = validateCapacityInplace(place, capasityRequested);
	if (result){
		this.classList.remove('errorInput');
		this.classList.add('allowInput');
		errors = false;
	}
	else{
		errors = true;
		this.classList.remove('allowInput');
		this.classList.add('errorInput');
	}
}

function printMessageError(){
	var notify = document.getElementById('alert');
    var textN = document.createTextNode("¡Error! Verifique los campos");
    var pTag = document.createElement("P");
	pTag.appendChild(textN);                                          
    notify.appendChild(pTag);
    notify.classList.add('error');
}

 function validateInputNumbers(valuePriceInset,valuePirceTicket,valueCapacity){
 	var result = false;
 	var isNumeric = [valuePriceInset,valuePirceTicket,valueCapacity];
 	for (var i = 0; i < isNumeric.length; i++) {
 		if(!validateNumber(isNumeric[i].value)){
 			showError(isNumeric[i]);
 			result = true;
 		}
 	};
 	return result;
 }
