loadtable();

function loadtable () {
  var eventsList = getAllEvents();
  var tbody = document.querySelector('#events tbody');
  var eventsListToShow = [];

for (var i = 0; i < eventsList.length; i++) {
	if(eventsList[i][10] === true ){
		eventsListToShow.push(eventsList[i]);
	}
	};

  tbody.innerHTML = '';
  for(var i = 0; i < eventsListToShow.length; i++){

    var newRow = tbody.insertRow(i);
    var eventName = newRow.insertCell();
    var dateBe = newRow.insertCell();
    var place = newRow.insertCell();
    var type = newRow.insertCell();
    var btns = newRow.insertCell();

    var btnEdit = document.createElement('input');
    btnEdit.type = 'button';
    btnEdit.value = 'Editar';
    btnEdit.name = eventsListToShow[i][0];
    btnEdit.classList.add('btnList');
    btnEdit.classList.add('btnImportant');
    btnEdit.addEventListener('click', editEvent);

    var btnDelete = document.createElement('input');
    btnDelete.type = 'button';
    btnDelete.value = 'Eliminar';
    btnDelete.name = eventsListToShow[i][0];
    btnDelete.classList.add('btnList');
    btnDelete.classList.add('btnImportant');
    btnDelete.addEventListener('click', deleteEvent);

    var btnAssing = document.createElement('input');
    btnAssing.type = 'button';
    btnAssing.value = 'Asignar';
    btnAssing.name = eventsListToShow[i][0];
    btnAssing.classList.add('btnList');
    btnAssing.classList.add('btnImportant');
    btnAssing.addEventListener('click', assingEvent);

    eventName.innerHTML = eventsListToShow[i][1];
    dateBe.innerHTML = eventsListToShow[i][2];
    place.innerHTML = eventsListToShow[i][5];
    type.innerHTML = eventsListToShow[i][8];
    btns.appendChild(btnEdit);
    btns.appendChild(btnDelete);
    btns.appendChild(btnAssing);
  }
}