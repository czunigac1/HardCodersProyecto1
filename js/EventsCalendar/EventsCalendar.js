    $('#calendar').fullCalendar({
        events: getEventsList(),
         eventClick: function() {
            showModal(this);
         },
         locale: 'es'
    });

var modal = document.getElementById('sellEvent');

var span = document.getElementsByClassName("close")[0];


var showModal = function(enventClicked) {
    var spanElemet = enventClicked.getElementsByTagName('span');
    var eventVal = spanElemet[0].innerHTML;
    var allEvent = getAllEvents();
    var allPlaces;
    var evenSelected;
    
    for (var i = 0; i < allEvent.length; i++) {
        if(allEvent[i][1] == eventVal){
            evenSelected = allEvent[i];
        }
    };

     modal.style.display = "block";
}

span.onclick = function() {
    modal.style.display = "none";
}

window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}