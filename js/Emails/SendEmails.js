(function() {
  $(document).ready(function() {
    var notify = document.getElementById('alert');
    notify.classList.remove('error');
    var textN = document.createTextNode("¡Error! Verifique sus datos");
    var pTag, pSuccessTag;    
    var removed = false, checked = false, nomore;

    return $('#sendMail').submit(function(e) {
     var email, message, name;
      name = document.getElementById('fullname');
      email = document.getElementById('email');
      message = document.getElementById('message');
      if (!name.value || !email.value || !message.value) {
      pTag = document.createElement("P");  
        if (typeof pSuccessTag != 'undefined' && !removed) {
        notify.removeChild(pSuccessTag);  
        notify.classList.remove('success'); 
        removed = true;
        nomore = false;
      };      
        pTag.appendChild(textN);                                          
        notify.appendChild(pTag);
        notify.classList.add('error');
        checked = false;
        return false;
      } else {
        $.ajax({
            url: "https://formspree.io/fcthardcoders@gmail.com", 
           method: "POST",
          data: $(this).serialize(),
          datatype: "json"
        });
        e.preventDefault();
        $(this).get(0).reset();
        return showSucces();
      }
    });
    function showSucces(){
      if (typeof pTag != 'undefined' && !checked) {
        notify.removeChild(pTag);  
        checked =true;
      };
      if(typeof pSuccessTag ==='undefined'){
       pSuccessTag = document.createElement("P");
       var textN = document.createTextNode("¡Mensaje enviado! Gracias por su mensaje");      
        pSuccessTag.appendChild(textN);                                          
        notify.appendChild(pSuccessTag);
        notify.classList.add('success'); 
        removed = false;
         nomore =true;
        }
        else if(removed && !nomore){
        pSuccessTag = document.createElement("P");
       var textN = document.createTextNode("¡Mensaje enviado! Gracias por su mensaje");      
        pSuccessTag.appendChild(textN);                                          
        notify.appendChild(pSuccessTag);
        notify.classList.add('success'); 
        removed = false;
        nomore= false;
        }
    }
  });

}).call(this);
