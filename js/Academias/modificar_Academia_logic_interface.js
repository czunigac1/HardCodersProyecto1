
llenarTablaModificar();
document.querySelector('#btnModificar').addEventListener('click',obtenerNuevaInformacion);
function llenarTablaModificar(){
    var listSchools=obtenerAcademias();
     var pid = getUrlVars()["id"];
    for(var i=0; i<listSchools.length;i++){
        if(listSchools[i][0] == pid){
            document.querySelector('#txtNombre').value=listSchools[i][1];
            document.querySelector('#txtUbicacion').value=listSchools[i][2];
            document.querySelector('#txtTelefono').value=listSchools[i][3];
            document.querySelector('#txtPersonaContacto').value=listSchools[i][4];
            document.querySelector('#txtCorreo').value=listSchools[i][5];
            
           }
      
    }
  
}
function obtenerNuevaInformacion(){
    var listNewInfoSchool=document.querySelectorAll("input[type=text]");
    var listNewInfoSchool2=[];
    var exitsError=false;
    var id;
  
    
    exitsError = validarCamposCompletos(listNewInfoSchool);
    
    if(exitsError == false){
       for(var i=0; i<listNewInfoSchool.length; i++){
            listNewInfoSchool2.push(listNewInfoSchool[i].value);
        }
        
        id=getUrlVars()["id"];
        editarInformacionAcademias(id,listNewInfoSchool2);
      redirecionarPaginaListar();
       }else{
        alert("Por favor, llenar todos los campos");
    }
    
    
    
    
    
}

function validarCamposCompletos(plistData){
     var exitsError = false;    
    
    for(var i=0; i<plistData.length; i++){
        if(plistData[i].value.length === 0 ){
            exitsError= true;
             }
       
    }
    
    return exitsError;
}


function redirecionarPaginaListar(){
    window.location.href='../../Views/Academias/listarAcademias.html';
}

function getUrlVars() {
     var vars = {};
     var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
          vars[key] = value;
     });
     return vars;
}