llenarTabla();
llenarListaNombresAcademias();
function llenarTabla(){
    
    
    var listSchools = obtenerAcademias();
    var tbody= document.querySelector('#tableSchools tbody');
    tbody.innerHTML= '';
    
      for(var i= 0; i < listSchools.length; i++){
          
          if(listSchools[i][6]=== true){
              var row = document.createElement('tr')
              var columnNameSchool = document.createElement('td'),
               columnPlace = document.createElement('td'),
               columnTelephone = document.createElement('td'),
               columnContactName = document.createElement('td'),
               columnEmail = document.createElement('td'),
               botonEdit = document.createElement('td'),
               linkEdit = document.createElement('button');
               linkEdit.addEventListener('click',editar); 
              linkEdit.classList.add('btnList');
              linkEdit.classList.add('btnImportant');
             var botonSpaceDelete = document.createElement('td'),
                 deleteBtn = document.createElement('button');
                 deleteBtn.classList.add('btnList');
                  deleteBtn.classList.add('btnImportant');
                deleteBtn.addEventListener('click',elimnar);
              var botonSpaceShowStudents = document.createElement('td'),
                  showListStudentsBtn = document.createElement('button'); 
                  showListStudentsBtn.classList.add('btnList');
                  showListStudentsBtn.classList.add('btnImportant');
                  showListStudentsBtn.addEventListener('click',verAtletas);
                 row.id=listSchools[i][0];    
                
          
          var nodoTextNameSchool = document.createTextNode(listSchools[i][1]),
              nodoTextPlace = document.createTextNode(listSchools[i][2]),
              nodoTextTelephone = document.createTextNode(listSchools[i][3]),
              nodoTextContacName = document.createTextNode(listSchools[i][4]),
              nodoTextEmail = document.createTextNode(listSchools[i][5]),
              nodoTextEdit = document.createTextNode('Modificar'),
              nodoTextDelete = document.createTextNode('Eliminar'),
              nodoTextShowStudents = document.createTextNode('Ver Estudiantes');
          
          
              columnNameSchool.appendChild(nodoTextNameSchool);
              columnPlace.appendChild(nodoTextPlace);
              columnTelephone.appendChild(nodoTextTelephone);
              columnContactName.appendChild(nodoTextContacName);
              columnEmail.appendChild(nodoTextEmail);
              linkEdit.appendChild(nodoTextEdit);
              botonEdit.appendChild(linkEdit);
              deleteBtn.appendChild(nodoTextDelete);
              botonSpaceDelete.appendChild(deleteBtn);
              showListStudentsBtn.appendChild(nodoTextShowStudents);
              botonSpaceShowStudents.appendChild(showListStudentsBtn);
              
            
            
          
             row.appendChild(columnNameSchool);
             row.appendChild(columnPlace);
             row.appendChild(columnTelephone);
             row.appendChild(columnContactName);
             row.appendChild(columnEmail);
             row.appendChild(botonSpaceShowStudents);
             row.appendChild(botonEdit);
             row.appendChild(botonSpaceDelete);
            
          
              tbody.appendChild(row);
              
          }
          
     
          
     
         
 }
    
    
}
 function editar(){

         var tr = this.parentElement.parentElement;
        var idSchool=tr.id;
      window.location.href='../../Views/Academias/modificarAcademia.html?id'+'='+idSchool;
     }
function elimnar(){
    
    var answer= confirm("¿Seguro que desea realizar está acción?\nPresione Aceptar para confirmar.")
    if(answer === true){
         var tr = this.parentElement.parentElement;
        var idSchool=tr.id;
        tr.classList.add('.Hidden');
        ocultarAcademia(idSchool);
        llenarTabla();
        
       }
}

function llenarListaNombresAcademias(){
    var listInfoSchoolToFind= obtenerAcademias();
    
    
   
    
}
function verAtletas(){
      var tr = this.parentElement.parentElement;
      var idSchool=tr.id;
        window.location.href='../../Views/Academias/lista_estudiantes_por_academia.html?id'+'='+idSchool;
}
